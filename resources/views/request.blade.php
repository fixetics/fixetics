<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Request A Service | Fixetics</title>
    <link rel="shortcut icon" href="favicon.html" type="image/x-icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fontawesome.min.css">
    <link rel="stylesheet" href="/fonts/flaticon.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/aos.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
        <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
<link rel="manifest" href="/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
    <body>        
        <div class="preloader">
            <div id="circle_square">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="header-4">
            <div class="container this-container">
                <div class="row">
                    <div class="col-xl-2 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-7 d-xl-block d-lg-block d-flex align-items-center">
                                <div class="logo">
                                    <a href="/">
                                        <img src="/img/logo.png" alt="fixetics">
                                    </a>
                                </div>
                            </div>
                            <div class="col-5 d-xl-none d-lg-none d-block">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-9">
                        <div class="mainmenu">
                            <nav class="navbar navbar-expand-lg">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    Home
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    About Us
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    Our Services
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    Request Quote
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="support-area">
                            <ul>
                                <li>
                                    <a href="#" class="user-button"><i class="fas fa-user"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="user-button"><i class="fas fa-phone"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="user-button"><i class="fas fa-envelope-open"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<div class="contact">
            <div class="container">
                <div class="row justify-content-around" style="padding:20px">
                    <div class="col-xl-12 col-lg-12">
                        <div class="contact-form">
                            <span class="subtitle">REQUEST A SERVICE QUOTE</span>
                            <h4>Need a better quote
                                    for our service?</h4>
                            <form action="/payment">
                                <input type="text" placeholder="Enter Your Full Name">
                                <input type="tel" placeholder="Enter Your Telephone">
                                <input type="email" placeholder="Enter Email Address">
                                
                                <select class="_service">
                                    <option disabled="" selected="">What Kind of Service You Want ?</option>
                                    <option>Setup My Device</option>
                                    <option>Repair My Device</option>
                                    <option>Support My Device</option>
                                </select>
                                <select class="_subService">
                                    <option disabled="" selected="">What Kind of Device You Want Set Up</option>
                                    <option>Television</option>
                                    <option>Computer/Laptop</option>
                                    <option>Mobile Phone</option>
                                    <option>Tablet</option>
                                    <option>Printer</option>
                                </select>
                                <textarea placeholder="Explain a bit more.."></textarea>

                            <button class="service_quote btn-murtes-6" disabled="
                            ">Quote For Service - £100</button><br/><br/>

                            <button id='book1' type="submit" class="btn-murtes-6">Book An Appointment<i class="fas fa-long-arrow-alt-right"></i></button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer footer-2 footer-4">
            <div class="container">
                <div class="row justify-content-between">
                    
                    <div class="col-xl-4 col-lg-6 col-md-4">
                        <div class="about-widget">
                            <h3>About Us</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>

                    <div class="col-xl-8 col-lg-6 col-md-8">
                        <div class="links-widget">
                            <h3>Quick Links</h3>
                            <ul>
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer end -->

        <!-- copyright begin -->
        <div class="copyright copyright-4">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2019 Fixetics | All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                                <li>
                                    <a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href="#"><i class="fab fa-skype"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/js/jquery-3.4.0.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/owl.carousel.min.js"></script>
        <script src="/js/jquery.magnific-popup.js"></script>
        <script src="/js/jquery.counterup.min.js"></script>
        <script src="/js/counter-us-activate.js"></script>
        <script src="/js/jquery.waypoints.js"></script>
        <script src="/js/wow.min.js"></script>
        <script src="/js/aos.js"></script>
        <script src="/js/main.js"></script>
        <script type="text/javascript">
            $(function() {
              $('._service').change(function() {
                $('._subService').show();
              }); 
                $('._subService').change(function() {
                    $('.service_quote').show();
              });

                // $('#book1').click(function() {
                //     $(location).attr("href","/payment")
                // });


            });
        </script>
    </body>
</html>