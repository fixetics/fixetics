<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fixetics</title>
    <link rel="shortcut icon" href="favicon.html" type="image/x-icon">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/fontawesome.min.css">
    <link rel="stylesheet" href="/fonts/flaticon.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/magnific-popup.css">
    <link rel="stylesheet" href="/css/aos.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/responsive.css">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/img/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/img/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/img/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/img/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/img/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/img/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/img/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/img/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
<link rel="manifest" href="/img/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/img/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
    <body>        
        <div class="preloader">
            <div id="circle_square">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
        <div class="header-4">
            <div class="container this-container">
                <div class="row">
                    <div class="col-xl-2 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-7 d-xl-block d-lg-block d-flex align-items-center">
                                <div class="logo">
                                    <a href="/">
                                        <img src="/img/logo.png" alt="fixetics">
                                    </a>
                                </div>
                            </div>
                            <div class="col-5 d-xl-none d-lg-none d-block">
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fas fa-bars"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-9">
                        <div class="mainmenu">
                            <nav class="navbar navbar-expand-lg">
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav ml-auto">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    Home
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    About Us
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="#">
                                                    Our Services
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="/request-a-service">
                                                    Request Quote
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="support-area">
                            <ul>
                                <li>
                                    <a href="#" class="user-button"><i class="fas fa-user"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="user-button"><i class="fas fa-phone"></i></a>
                                </li>
                                <li>
                                    <a href="#" class="user-button"><i class="fas fa-envelope-open"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-4">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-center justify-content-center">
                    <div class="col-xl-5 col-lg-9 col-md-8 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="banner-content">
                            <h1><span class="special">Community</span> driven Tech
                                Service & Maintenance</h1>
                            <p></p>
                            <a href="#" class="btn-murtes banner-button">Know more <i class="fas fa-long-arrow-alt-right"></i></a> 
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5">
                        <div class="part-img">
                            <img src="/img/banner.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="statics">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="500">
                        <div class="single-statics">
                            <span class="number"><span class="counter">25</span>+</span>
                            <span class="title">Years of experience</span>
                            <div class="bg-icon">
                                <img src="/img/svg/timetable.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="1000">
                        <div class="single-statics">
                            <span class="number"><span class="counter">98</span>+</span>
                            <span class="title">Total project</span>
                            <div class="bg-icon">
                                <img src="/img/svg/contract.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="1500">
                        <div class="single-statics">
                            <span class="number"><span class="counter">35</span>+</span>
                            <span class="title">Winning awards</span>
                            <div class="bg-icon">
                                <img src="/img/svg/trophy.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3" data-aos="new-animation" data-aos-delay="100" data-aos-duration="2000">
                        <div class="single-statics">
                            <span class="number"><span class="counter">84</span>+</span>
                            <span class="title">Happy clients</span>
                            <div class="bg-icon">
                                <img src="/img/svg/happiness.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="choosing-reason-4">
            <div class="container p-0">
                <div class="row no-gutters">
                    <div class="col-xl-6 col-lg-6">
                        <div class="part-img"></div>
                    </div>
                    <div class="col-xl-6 col-lg-12">
                        <div class="part-reasons">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6 col-md-6" data-aos="zoom-out-up" data-aos-delay="100" data-aos-duration="600">
                                    <div class="single-reason">
                                        <h3>24/7 customer
                                                support</h3>
                                        <p>How all this mistaken<br>
                                                was born & because those<br>
                                                who do not know.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6" data-aos="zoom-out-up" data-aos-delay="100" data-aos-duration="600">
                                    <div class="single-reason">
                                        <h3>Awesome
                                                team member</h3>
                                        <p>How all this mistaken<br>
                                                was born & because those<br>
                                                who do not know.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6" data-aos="zoom-out-up" data-aos-delay="100" data-aos-duration="600">
                                    <div class="single-reason">
                                        <h3>Good product
                                                quality</h3>
                                        <p>How all this mistaken<br>
                                                was born & because those<br>
                                                who do not know.</p>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6" data-aos="zoom-out-up" data-aos-delay="100" data-aos-duration="600">
                                    <div class="single-reason">
                                        <h3>Many years
                                                exprience</h3>
                                        <p>How all this mistaken<br>
                                                was born & because those<br>
                                                who do not know.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--         <div class="service-2 service-4">
            <div class="container">
                <div class="this-section-title">
                    <div class="row justify-content-between">
                        <div class="col-xl-6 col-lg-6">
                            <h2>We creating solutions<br/>
                                    for your organization</h2>
                        </div>
                    </div>
                </div>
                
                <div class="service-2-slider owl-carousel owl-theme">
                    <div class="single-servcie">
                        <div class="left">
                            <div class="number">01.</div>
                        </div>
                        <div class="right">
                            <h3 class="service-title">Warranty<br/> managment it</h3>
                            <p class="service-content">must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born</p>
                            <a href="#" class="service-details-button">details <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="single-servcie">
                        <div class="left">
                            <div class="number">02.</div>
                        </div>
                        <div class="right">
                            <h3 class="service-title">Product control service</h3>
                            <p class="service-content">must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born</p>
                            <a href="#" class="service-details-button">details <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="single-servcie">
                        <div class="left">
                            <div class="number">03.</div>
                        </div>
                        <div class="right">
                            <h3 class="service-title">Quality control system</h3>
                            <p class="service-content">must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born</p>
                            <a href="#" class="service-details-button">details <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="single-servcie">
                        <div class="left">
                            <div class="number">04.</div>
                        </div>
                        <div class="right">
                            <h3 class="service-title">Software Engineering</h3>
                            <p class="service-content">must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born</p>
                            <a href="#" class="service-details-button">details <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="single-servcie">
                        <div class="left">
                            <div class="number">05.</div>
                        </div>
                        <div class="right">
                            <h3 class="service-title">Desktop Computing</h3>
                            <p class="service-content">must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born</p>
                            <a href="#" class="service-details-button">details <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="single-servcie">
                        <div class="left">
                            <div class="number">06.</div>
                        </div>
                        <div class="right">
                            <h3 class="service-title">UI/UX <br/> Strategy</h3>
                            <p class="service-content">must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born</p>
                            <a href="#" class="service-details-button">details <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 -->
<!--         <div class="about-3 about-4">
            <div class="container">
                <div class="row justify-content-xl-between justify-content-lg-between justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-8 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="part-text">
                            <h2>
                                <span class="special">
                                25 YEARS EXPERIENCE
                                </span>
                                Before proceed,
                                learn about team.
                            </h2>
                            <p>Lorem Ipsum is simply dummy text of them printing typesetting
                            has been the industry's standard dummy text ever since the and
                            specimen book five centuries.</p>
                            <p>Lorem Ipsum is simply dummy text of them printing typesetting
                                    has been the industry's standard dummy text ever since the and
                                    specimen book five centuries.</p>
                        </div>
                    </div>
                    
                    <div class="col-xl-6 col-lg-6">
                        <div class="part-img">
                            <img class="main-img" src="/img/about-4.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

        <div class="case-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-8">
                        <div class="section-title-2 text-center">
                            <h2>Our Services</h2>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-sm-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="500" data-aos-easing="ease-in-sine">
                        <div class="single-case">
                            <div class="part-img">
                                <img src="/img/service1.jpg" alt="">
                                <div class="content-on-img">
                                    <h3>Setup<br/>Your Device</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-sm-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-case">
                            <div class="part-img">
                                <img src="/img/service2.jpg" alt="">
                                <div class="content-on-img">
                                    <h3>Repair<br/>Your Device</h3>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-sm-6" data-aos="murtes-animation" data-aos-offset="300" data-aos-duration="1500" data-aos-easing="ease-in-sine">
                        <div class="single-case">
                            <div class="part-img">
                                <img src="/img/service3.jpg" alt="">
                                <div class="content-on-img">
                                    <h3>Support<br/>Your Device</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!--                 <div class="row">
                    <div class="col-xl-12 col-lg-12">
                        <div class="view-button">
                            <a href="#" class="btn-murtes">More Cases <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                </div> -->

            </div>
        </div>

        <div class="testimonial-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-6 col-md-8">
                        <div class="section-title-2 text-center">
                            <h2>See what our<br/>
                                    customers say<br/>
                                    about us.</h2>
                            <p>All the Lorem Ipsum generators on the Internet tend to
                                    generator on theuses latin words.</p>
                        </div>
                    </div>
                </div>

                <div class="testimonial-3-slide owl-carousel owl-theme">
                    <div class="single-testimonial">
                        <div class="row">
                            <div class="col-xl-4 col-lg-5 col-md-6 offset-xl-1 offset-lg-0 offset-0">
                                <div class="part-img">
                                    <img src="/img/testi-user-2.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 offset-xl-1 offset-lg-1 offset-0">
                                <div class="part-text">
                                    <div class="text-body">
                                        <div class="rate">
                                            <ul>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fas fa-star"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>It is a long established fact that a reader will be
                                            distracted by the readable content of a page when
                                            looking at its layout. The point of using Lorem Ipsum
                                            is that it has a more-or-less normal distribution of
                                            letters, as opposed to using 'Content here, content
                                            making it look like readable.</p>
                                    </div>
                                    <div class="user-data">
                                        <span class="name">Kathleen Romero</span>
                                        <span class="position">CEO company</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial">
                        <div class="row">
                            <div class="col-xl-4 col-lg-5 col-md-6 offset-xl-1 offset-lg-0 offset-0">
                                <div class="part-img">
                                    <img src="/img/testi-user-2.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 offset-xl-1 offset-lg-1 offset-0">
                                <div class="part-text">
                                    <div class="text-body">
                                        <div class="rate">
                                            <ul>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fas fa-star"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>It is a long established fact that a reader will be
                                            distracted by the readable content of a page when
                                            looking at its layout. The point of using Lorem Ipsum
                                            is that it has a more-or-less normal distribution of
                                            letters, as opposed to using 'Content here, content
                                            making it look like readable.</p>
                                    </div>
                                    <div class="user-data">
                                        <span class="name">Kathleen Romero</span>
                                        <span class="position">CEO company</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="single-testimonial">
                        <div class="row">
                            <div class="col-xl-4 col-lg-5 col-md-6 offset-xl-1 offset-lg-0 offset-0">
                                <div class="part-img">
                                    <img src="/img/testi-user-2.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 offset-xl-1 offset-lg-1 offset-0">
                                <div class="part-text">
                                    <div class="text-body">
                                        <div class="rate">
                                            <ul>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li class="rated">
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fas fa-star"></i>
                                                </li>
                                                <li>
                                                    <i class="fas fa-star"></i>
                                                </li>
                                            </ul>
                                        </div>
                                        <p>It is a long established fact that a reader will be
                                            distracted by the readable content of a page when
                                            looking at its layout. The point of using Lorem Ipsum
                                            is that it has a more-or-less normal distribution of
                                            letters, as opposed to using 'Content here, content
                                            making it look like readable.</p>
                                    </div>
                                    <div class="user-data">
                                        <span class="name">Kathleen Romero</span>
                                        <span class="position">CEO company</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="price-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6 col-lg-8 col-md-8">
                        <div class="section-title-2 text-center">
                            <h2>Pricing</h2>
                            <p>Check out our best-in-industry affordable plans</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="fade-up" data-aos-offset="300" data-aos-duration="1000" data-aos-easing="ease-in-sine">
                        <div class="single-price">
                            <div class="part-head">
                                <span class="title">Basic User</span>
                                <span class="price">$860<small>/mo</small></span>
                            </div>
                            <div class="part-body">
                                <ul>
                                    <li>10GB Hosting Per Year</li>
                                    <li>30+ Email Address</li>
                                    <li>Unlimited Traffic Per Year</li>
                                    <li>Forum Access Per Year</li>
                                    <li>Online Support 24/7</li>
                                </ul>
                                <a href="#">Purchase Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="fade-up" data-aos-offset="300" data-aos-duration="1500" data-aos-easing="ease-in-sine">
                        <div class="single-price active">
                            <div class="part-head">
                                <span class="title">Basic User</span>
                                <span class="price">$860<small>/mo</small></span>
                            </div>
                            <div class="part-body">
                                <ul>
                                    <li>10GB Hosting Per Year</li>
                                    <li>30+ Email Address</li>
                                    <li>Unlimited Traffic Per Year</li>
                                    <li>Forum Access Per Year</li>
                                    <li>Online Support 24/7</li>
                                </ul>
                                <a href="#">Purchase Now</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6" data-aos="fade-up" data-aos-offset="300" data-aos-duration="2000" data-aos-easing="ease-in-sine">
                        <div class="single-price">
                            <div class="part-head">
                                <span class="title">Basic User</span>
                                <span class="price">$860<small>/mo</small></span>
                            </div>
                            <div class="part-body">
                                <ul>
                                    <li>10GB Hosting Per Year</li>
                                    <li>30+ Email Address</li>
                                    <li>Unlimited Traffic Per Year</li>
                                    <li>Forum Access Per Year</li>
                                    <li>Online Support 24/7</li>
                                </ul>
                                <a href="#">Purchase Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="support support-3">
            <div class="container">
                <div class="row justify-content-between">
                    
                    <div class="col-xl-5 col-lg-5 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="part-text">
                            <span class="phone-number">+44 7370 123532</span>
                           
                            
                            <a href="#" class="support-button">Contact now <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="part-cta">
                            <a href="#" class="cta-button">CALL FOR ADVICE NOW</a>
                            <h2>To make requests
                                for further information,
                                contact us via our social
                                channels.</h2>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer footer-2 footer-4">
            <div class="container">
                <div class="row justify-content-between">
                    
                    <div class="col-xl-4 col-lg-6 col-md-4">
                        <div class="about-widget">
                            <h3>About Us</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                        </div>
                    </div>

                    <div class="col-xl-8 col-lg-6 col-md-8">
                        <div class="links-widget">
                            <h3>Quick Links</h3>
                            <ul>
                                <li>
                                    <a href="#">Home</a>
                                </li>
                                <li>
                                    <a href="#">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#">Terms & Conditions</a>
                                </li>
                                <li>
                                    <a href="#">Privacy Policy</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- footer end -->

        <!-- copyright begin -->
        <div class="copyright copyright-4">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-xl-6 col-lg-6 d-xl-flex d-lg-flex d-block align-items-center">
                        <div class="cp-area">
                            <p>Copyright © 2019 Fixetics | All Rights Reserved</p>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6">
                        <div class="social-area">
                            <ul>
                                <li>
                                    <a class="facebook" href="#"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a class="twitter" href="#"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a class="skype" href="#"><i class="fab fa-skype"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/js/jquery-3.4.0.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/owl.carousel.min.js"></script>
        <script src="/js/jquery.magnific-popup.js"></script>
        <script src="/js/jquery.counterup.min.js"></script>
        <script src="/js/counter-us-activate.js"></script>
        <script src="/js/jquery.waypoints.js"></script>
        <script src="/js/wow.min.js"></script>
        <script src="/js/aos.js"></script>
        <script src="/js/main.js"></script>
    </body>
</html>