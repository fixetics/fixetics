# Fixetics - 

Website Mock as a part assessment submission  - Consultancy and Tech Innovation

## Built With

* [Laravel](https://laravel.com/) - PHP Framework
* [HTML], [CSS3], [JavaScript]


## Authors

* **Ashish Sharma** - [21396375](http://www.ashishsharma.in)
* **Mani Bakhtiari** - [21386828]
* **Naaz Fatima** - [21386828]


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Taylor Otwell - At Laravel

